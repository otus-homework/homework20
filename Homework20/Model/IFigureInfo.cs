﻿using Homework20.Figure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework20.Model
{
    interface IFigureInfo
    {
        void ShowAllFigureInfo(IFigure figure);
    }
}
