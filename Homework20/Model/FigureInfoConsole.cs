﻿using Homework20.Figure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework20.Model
{
    class FigureInfoConsole: IFigureInfo
    {
        public void ShowAllFigureInfo(IFigure figure)
        {
            ShowName(figure);
            ShowArea(figure);
            ShowPerimetr(figure);           
        }

        private void ShowName(IFigure figure)
        {
            Console.WriteLine($"Название фигуры: {figure.Name}");
        }

        private void ShowArea(IArea figure)
        {
            Console.WriteLine($"Площадь фигуры: {figure.Area}");
        }

        private void ShowPerimetr(IPerimetr figure)
        {
            Console.WriteLine($"Периметр фигуры: {figure.Periment}");
        }
    }
}
