﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework20.Figure
{
    class Rectangle : IFigure
    {
        public double Height { get; set; }
        public double Wight { get; set; }

        public Rectangle(double height, double wight)
        {
            Height = height;
            Wight = wight;
        }

        public string Name { get { return nameof(Rectangle); } }

        public double Area { get { return Height * Wight; } }

        public double Periment { get { return Height + Wight; } }
    }
}
