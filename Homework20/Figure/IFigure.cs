﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework20.Figure
{
    interface IFigure: IArea, IPerimetr
    {
        string Name { get; }
    }
}
