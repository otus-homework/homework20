﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework20.Figure
{
    class Circle : IFigure
    {
        public double Radius { get; set; }
        public string Name { get { return nameof(Circle); } }

        public Circle(double radius)
        {
            Radius = radius;
        }

        public double Area { get { return Math.PI * Math.Pow(Radius, 2); } }
        public double Periment { get { return 2 * Math.PI * Radius; } }
    }
}
