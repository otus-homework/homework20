﻿using Homework20.Model;
using Homework20.Figure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework20
{
    class Program
    {
        static void Main(string[] args)
        {
            IFigure circle = new Circle(3);
            IFigure rectangle = new Rectangle(3, 4);

            IFigureInfo figureInfo = new FigureInfoConsole();

            figureInfo.ShowAllFigureInfo(circle);
            figureInfo.ShowAllFigureInfo(rectangle);

            Console.ReadKey();
        }
    }
}
